#!/bin/bash

initialize() {
    if [ -d /uwsgi_conf.d ] && [ ! "$(find /etc/uwsgi/conf.d/ -name \*.ini -type f  | wc -l)" -gt 0 ]; then  
        # Copy startup supplied configuration on fresh start
        echo "Copying uwsgi supplied extra configuration files."
        cp -r /uwsgi_conf.d/* /etc/uwsgi/conf.d/
    fi
}

initialize