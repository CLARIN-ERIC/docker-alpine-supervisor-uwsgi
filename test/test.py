import requests                          

def application(environ, start_response):                         
    start_response('200 OK', [('Content-Type', 'text/plain')])
    return [str.encode('UWSGI server says: Yay!' , 'utf-8')]
